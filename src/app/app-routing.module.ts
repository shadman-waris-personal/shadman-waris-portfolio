import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomepageComponent } from "./homepage/homepage.component";
import { ResumePageComponent } from "./resume-page/resume-page.component";

const routes: Routes = [
  { path: "", component: HomepageComponent },
  { path: "resume", component: ResumePageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
