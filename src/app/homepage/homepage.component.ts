import { Component, OnInit } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-homepage",
  templateUrl: "./homepage.component.html",
  styleUrls: ["./homepage.component.scss"]
})
export class HomepageComponent implements OnInit {
  showProject: boolean = false;
  moreButton: boolean = true;
  lessButton: boolean = false;

  constructor(private spinner: NgxSpinnerService) {}

  ngOnInit() {
    this.spinner.show();

    setTimeout(() => {
      this.spinner.hide();
    }, 2000);
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
      var currentScrollPos = window.pageYOffset;
      if (prevScrollpos > currentScrollPos) {
        document.getElementById("navbar").style.top = "0";
        document.getElementById("mobile-navbar").style.top = "0";
      } else {
        document.getElementById("navbar").style.top = "-56px";
        document.getElementById("mobile-navbar").style.top = "-65px";
      }
      prevScrollpos = currentScrollPos;
    };

    let i;
    let divs = document.getElementsByClassName("text-regulator");
    for (i = 0; i < divs.length; i++) {
      if (divs[i].innerHTML != null) {
        divs[i].innerHTML = divs[i].innerHTML.substr(0, 175) + "...";
      }
    }
  }

  showMoreProjects() {
    this.showProject = true;
    this.moreButton = false;
    this.lessButton = true;
  }

  showLessProjects() {
    this.showProject = false;
    this.moreButton = true;
    this.lessButton = false;
  }

  showMobileNav() {
    console.log("clicked");
    let x = document.getElementById("mobile-top-navigation");
    if (x.style.display === "block") {
      x.style.display = "none";
      x.style.color = "#fff";
    } else {
      x.style.display = "block";
    }
  }

  hideMobileNav() {
    let x = document.getElementById("mobile-top-navigation");
    x.style.display = "none";
  }
}
